package edu.westga.cs1302.checkdigit.controller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is the controller class for the checkdigit application
 * 
 * @author Kathleen Anderson
 * @version 7/11/2021
 */
public class DigitController {
	private Scanner theScanner;
	private ArrayList<String> theList;
	
	/**
	 * Constructor for the controller
	 * 
	 * @param theFile
	 * @precondition theFile is a valid File
	 * @postcondition the DigitController object is ready to use
	 */
	public DigitController(String fileName) {
		this.theList = new ArrayList<String>();
		try {
			this.theScanner = new Scanner(new File(fileName));
		} catch (FileNotFoundException theException) {
			System.out.println("Input file not found;");
		}
	}
	
	/**
	 * Method to run the program
	 * 
	 * @precondition None
	 * @postcondition The program has run
	 */
	public void run() {
		String result = "";
		if (this.theScanner == null) {
			result += "File not found";
		} else {
			this.read();
			for (String currentString : this.theList) {
				result += currentString + "\n";
			}
			result.trim();
			this.write("accounts.txt", result);
		}
		System.out.println(result);
	}
	
	/**
	 * Reads and processes the file's contents
	 * 
	 * @precondition None
	 * @postcondition The file's contents have been processed
	 */
	public void read() {
		String[] currentLine;
		while (this.theScanner.hasNextLine()) {
			currentLine = this.theScanner.nextLine().split(",");
			for (String current : currentLine) {
				this.theList.add(this.getCheckDigit(Integer.parseInt(current)));
			}
		}
	}
	
	/**
	 * Creates a file with the given name and writes the given contents in it
	 * 
	 * @param filename The name of the file
	 * @param contents The contents of the file
	 */
	public void write(String filename, String contents) {
		try {
			FileWriter theWriter = new FileWriter(filename);
			theWriter.write(contents);
			theWriter.flush();
			theWriter.close();
		} catch (IOException theException) {
			System.out.println("File writing error");
		}
	}
	
	/**
	 * Returns the number with its check digit appended as a string
	 * 
	 * @param input The number that needs a check digit
	 * @precondition None
	 * @return The number with its check digit appended
	 */
	public String getCheckDigit(int input) {
		return String.valueOf(input) + (input % 3);
	}
}
