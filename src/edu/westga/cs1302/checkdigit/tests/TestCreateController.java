package edu.westga.cs1302.checkdigit.tests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.checkdigit.controller.DigitController;

class TestCreateController {
	DigitController theController;
	
	@Test
	void testGetCheckDigit() {
		theController = new DigitController("pure.txt");
		assertEquals(this.theController.getCheckDigit(1234), "12341");
	}

}
