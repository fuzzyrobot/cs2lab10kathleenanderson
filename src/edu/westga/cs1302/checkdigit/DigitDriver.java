package edu.westga.cs1302.checkdigit;

import edu.westga.cs1302.checkdigit.controller.DigitController;

/**
 * This is the driver class for the checkdigit application
 * 
 * @author Kathleen Anderson
 * @version 7/11/2021
 */
public class DigitDriver {

	/**
	 * Entry point to the application
	 * 
	 * @param args Used to accept command line arguments
	 */
	public static void main(String[] args) {
		DigitController theController = new DigitController("pure.txt");
		theController.run();
	}

}
